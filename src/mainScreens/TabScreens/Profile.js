import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  Image,
  TouchableOpacity,
  Alert,
} from 'react-native';
import React, {useEffect} from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import AuthFrame from '../../authScreens/AuthFrame';
import {Rtext} from '../../common/Rtext';
import {
  AllIcons,
  baseUrlRb,
  colors,
  startAudioRecord,
} from '../../assets/common/Common';
import {CusButtom} from '../../common/CusButtom';
import {useDispatch, useSelector} from 'react-redux';
import {textToSpeech, userLogout} from '../../Store/AuthReducer';
import {Ainput} from '../../common/Ainput';
import {useState} from 'react';
import CustomBack from '../../common/CustomBack/CustomBack';
import {useNavigation} from '@react-navigation/native';
import {newPlayer} from '../../helperClasses/Player';
import {request} from '../../utility/common';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {FingerPrint} from '../../component/FingerPrint';
import {NewTextToSpeech} from '../../component/TextToSpeech';
import SpeechToText from '../../component/SpeechToText';
const {height, width} = Dimensions.get('window');
export default function Profile({}) {
  const navigation = useNavigation();
  const language = useSelector(store => store?.auth?.language);
  const LanguageCheck = () => {
    return language === 'hindi';
  };
  const dispatch = useDispatch();
  const auth = useSelector(store => store?.auth);
  const [userData, setUserData] = useState({});
  const [pin, setPin] = useState('');
  const [isCalled, setIsCalled] = useState(false);
  const [speechToText, partial, results, speechError, _destroyRecognizer] =
    SpeechToText();

  const getUserData = () => {
    let data = auth.userData.replace(/'/g, '"');
    data = data.replace(/ObjectId/, '');
    data = data.replace(/\)/, '');
    data = data.replace(/\(/, '');
    console.log('data', JSON.parse(data).full_name);
    setUserData(JSON.parse(data));
    setPin(JSON.parse(data).pin_number);
  };
  const playtheUserInfo = () => {
    let data = auth.userData.replace(/'/g, '"');
    data = data.replace(/ObjectId/, '');
    data = data.replace(/\)/, '');
    data = data.replace(/\(/, '');
    data = JSON.parse(data);

    NewTextToSpeech.readText(
      `your Nick name is ${data.nick_name}, your Full name is ${data.full_name} , your User name is ${data.user_name} , your mobile number is ${data.mob_number} , your U p i i d is ${data.upi_id} `,
    );
  };

  const updateProfile = () => {
    if (pin === '') {
      Alert.alert('wront pin input');
      dispatch(
        textToSpeech({
          input: `Wrong pin number`,
          gender: 'female',
          lang: 'english',
          alpha: 1,
          segmentwise: 'True',
        }),
      );
    }
    request('post', baseUrlRb, {
      action: 'update',
      nick_name: userData.nick_name,
      key: 'pin_number',
      value: pin,
    })
      .then(res => {
        dispatch(
          textToSpeech({
            input: `your pin updated successfully`,
            gender: 'female',
            lang: 'english',
            alpha: 1,
            segmentwise: 'True',
          }),
        ).then(() => {
          setTimeout(() => {
            Alert.alert('Updated successfully');
            navigation.goBack();
          }, 2000);
        });
      })
      .catch(error => {
        dispatch(
          textToSpeech({
            input: `somthing want wrong`,
            gender: 'female',
            lang: 'english',
            alpha: 1,
            segmentwise: 'True',
          }),
        );
      });
  };

  useEffect(() => {
    newPlayer.stopPlaying();
    // playtheUserInfo()
    getUserData();
  }, []);
  useEffect(() => {
    console.log('profile', results);
    console.log('err', speechError);

    if (speechError.length > 0) {
      NewTextToSpeech.readText('Audio not detected please try again.');
    } else if (results.length > 0) {
      if (results[0].toLocaleLowerCase().includes('information')) {
        playtheUserInfo();
      } else if (results[0].toLocaleLowerCase().includes('delete')) {
        deleteAccount();
      } else if (results[0].toLocaleLowerCase().includes('money')) {
        navigation.navigate('Pay');
      } else {
        NewTextToSpeech.readText(
          results[0] + '  is a wrong option tap the screen to retry',
        );
      }
      setIsCalled(false);
      _destroyRecognizer();
    }
  }, [speechError, results]);

  const deleteAccount = () => {
    request('post', baseUrlRb, {
      action: 'remove',
      nick_name: userData.nick_name,
    }).then(() => {
      NewTextToSpeech.readText('your account deleted successfully');
      dispatch(userLogout());
    });
  };

  const OptionsForUser = () => {
    if (isCalled) {
      return;
    }
    setIsCalled(true);
    NewTextToSpeech.readText(
      'Please select one option , for account information say information,  for delete account say delete account.',
    );
    setTimeout(() => {
      speechToText();
    }, 7000);
  };

  return (
    <SafeAreaView style={styles.container}>
      <AuthFrame>
        <KeyboardAwareScrollView>
          <TouchableOpacity
            activeOpacity={1}
            onPress={() => OptionsForUser()}
            style={styles.container}>
            <View>
              <Image
                style={{
                  height: 80,
                  width: 80,
                  backgroundColor: colors.white,
                  borderRadius: 100,
                  alignSelf: 'center',
                  marginBottom: 20,
                  marginTop: 40,
                }}
                source={AllIcons.logoIcon}
              />
            </View>
            <View
              style={{
                justifyContent: 'space-between',
                marginHorizontal: width * 0.03,
              }}>
              <Ainput
                editable={false}
                containerStyle={{width: width * 0.94}}
                placeholder={LanguageCheck() ? 'निक नाम' : 'Nick name'}
                heading={LanguageCheck() ? 'निक नाम' : 'Nick name'}
                value={userData.nick_name}
              />
              <Ainput
                editable={false}
                containerStyle={{width: width * 0.94}}
                placeholder={'Full name'}
                heading={LanguageCheck() ? 'पूरा नाम' : 'Full name'}
                value={userData.full_name}
              />
              <Ainput
                editable={false}
                containerStyle={{width: width * 0.94}}
                placeholder={'User name'}
                heading={LanguageCheck() ? 'उपयोगकर्ता नाम' : 'User name'}
                value={userData.user_name}
              />
              <Ainput
                containerStyle={{width: width * 0.94}}
                placeholder={'Pin number'}
                heading={LanguageCheck() ? 'पिन नंबर' : 'Pin number'}
                value={pin}
                onChangeText={setPin}
              />
              <Ainput
                editable={false}
                containerStyle={{width: width * 0.94}}
                placeholder={'Mob number'}
                heading={LanguageCheck() ? 'मोबाइल नंबर' : 'Mobile number'}
                value={userData.mob_number}
              />
              <Ainput
                editable={false}
                containerStyle={{width: width * 0.94}}
                placeholder={'Upi id'}
                heading={LanguageCheck() ? 'यूपीआई आईडी' : 'Upi id'}
                value={userData.upi_id}
              />
            </View>
            <CusButtom
              onpress={() => {
                navigation.navigate('FaceDetection');
              }}
              text={LanguageCheck() ? 'चेहरे का पहचान' : 'Face Detection'}
              BTNstyle={{
                marginHorizontal: 40,
                borderRadius: 100,
                backgroundColor: colors.appColor,
              }}
            />
            <CusButtom
              onpress={() => {
                FingerPrint();
              }}
              text={LanguageCheck() ? 'अद्यतन' : 'FingerPrint'}
              BTNstyle={{
                marginHorizontal: 40,
                borderRadius: 100,
                backgroundColor: colors.appColor,
              }}
            />
            <CusButtom
              onpress={() => {
                updateProfile();
              }}
              text={LanguageCheck() ? 'अद्यतन' : 'Update'}
              BTNstyle={{
                marginHorizontal: 40,
                borderRadius: 100,
                backgroundColor: colors.appColor,
              }}
            />
            <CusButtom
              onpress={() => {
                deleteAccount();
              }}
              text={LanguageCheck() ? 'खाता हटा दो' : 'Delete account'}
              BTNstyle={{
                marginHorizontal: 40,
                borderRadius: 100,
                backgroundColor: colors.appColor,
              }}
            />
            <CusButtom
              onpress={() => {
                dispatch(userLogout());
              }}
              text={LanguageCheck() ? 'लॉग आउट' : 'Logout'}
              BTNstyle={{
                marginHorizontal: 40,
                borderRadius: 100,
                backgroundColor: colors.appColor,
              }}
            />
            {/* <CusButtom onpress={() => dispatch(userLogout())} textStyle={styles.txt} text={"Logout"} BTNstyle={styles.logout} /> */}
          </TouchableOpacity>
        </KeyboardAwareScrollView>
      </AuthFrame>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingBottom: 200,
  },
  commingSoon: {
    fontSize: 22,
    fontWeight: 'bold',
    color: colors.white,
  },
  soon: {
    fontSize: 16,
    color: colors.white,
  },
  logout: {
    width: '80%',
    right: '10%',
    height: 48,
    position: 'absolute',
    bottom: 150,
    backgroundColor: colors.appColor,
    borderRadius: 100,
    borderWidth: 2,
  },
  txt: {
    fontSize: 14,
    color: colors.white,
  },
});
