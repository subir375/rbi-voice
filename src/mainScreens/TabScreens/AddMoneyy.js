import { View, Text, StyleSheet, Image, TextInput, Dimensions, Alert } from 'react-native'
import React, { useEffect, useState } from 'react'
import { SafeAreaView } from 'react-native-safe-area-context';
import AuthFrame from '../../authScreens/AuthFrame';
import CustomBack from '../../common/CustomBack/CustomBack';
import { AllIcons, colors, keyBoardType } from '../../assets/common/Common';
import { Rtext } from '../../common/Rtext';
import { Ainput } from '../../common/Ainput';
import { LeftHeadingTxt } from './Home';
import { CusButtom } from '../../common/CusButtom';
import RazorpayCheckout from 'react-native-razorpay';
import { useIsFocused } from '@react-navigation/native';
export default function AddMoneyy(props) {
  const [money, setMoney] = useState("");

const isFocused = useIsFocused()
  useEffect(() =>{
    if(isFocused){
      setMoney("");
    }
  },[isFocused])
  return (
    <SafeAreaView>
      <AuthFrame>
        <CustomBack name='Add Money' />
        <View style={styles.mainContainer}>
          <Rtext style={styles.wallet}>Your Wallet Balance</Rtext>
        </View>
        <View style={{ flexDirection: 'row', paddingHorizontal: "5%" }}>
          <Image style={{ height: 20, width: 20, resizeMode: 'stretch', marginTop: 25, tintColor: colors.white }} source={AllIcons.paisa} />
          <Rtext style={styles.price}>50000.00</Rtext>
        </View>
        <View style={{ paddingHorizontal: "5%" }}>
          <Rtext style={styles.enterText}>Enter Amount</Rtext>
          <PaisaTxtInput value={money} onChangeText={setMoney} />

          <Rtext style={{ ...styles.enterText, marginTop: 10 }}>Last Transaction</Rtext>
          <View style={styles.transectionView}>
            {
              [100, 200, 100, 100].map((item, index) => (
                <View style={{ flexDirection: 'row', alignItems: 'center', backgroundColor: colors.white, marginRight: (index !== 0 && index % 3 === 0) ? 0 : 5, paddingHorizontal: 15, paddingVertical: 2, borderRadius: 20, marginTop: 5 }}>
                  <Image style={styles.icon} source={AllIcons.paisa} />
                  <Rtext style={styles.pricetxt}>{item}</Rtext>
                </View>
              ))
            }
          </View>

        </View>
        <CusButtom onpress={() => {



          let newMoney = 0.0
          try {
            newMoney = parseFloat(money);
            if (newMoney <= 0 || money === "" || money === "," || money.includes(" ") === true) {
              Alert.alert("enter a amount", "You have to atleast one rupee")
              return
            }
            // if (newMoney > 50000.01) {
            //   Alert.alert("insufficient money", "Please add less then 50000")
            //   return
            // }
          } catch (error) {
            Alert.alert("enter a amount", "You have to atleast one rupee")
          }

          var options = {
            key: "rzp_test_VdGdvprTKB8u1w",
            currency: "INR",
            amount: 100 * newMoney,
            name: "Users Name",
            description: "Thanks for purchasing",
            prefill: {
              name: "RBI",
            },
            theme: { color: '#F37254' }
          }
          RazorpayCheckout.open(options).then((data) => {
            props.navigation.navigate('Success')
          }).catch((error) => {
            alert(`Error: ${error.code} | ${error.description}`);
          });
        }} textStyle={styles.denoTxt} text={"Auto Select Denominations"} BTNstyle={styles.denoBtn} />
        <CusButtom onpress={() => {
          
          let newMoney = 0.0
          try {
            newMoney = parseFloat(money);
            if (newMoney <= 0 || money === "" || money === "," || money.includes(" ") || money === ".") {
              setMoney("")
              Alert.alert("enter a amount", "You have to atleast one rupee")
              return
            }
            // if (newMoney > 50000.01) {
            //   Alert.alert("insufficient money", "Please add less then 500")
            //   return
            // }
            props.navigation.navigate('AddMoneyDeno', { newMoney })
          } catch (error) {
            Alert.alert("enter a amount", "You have to atleast one rupee")
          }


        }} textStyle={styles.btnTxt} text={"Select Denominations"} BTNstyle={styles.submitBtn} />
      </AuthFrame>
    </SafeAreaView>
  )
}


const PaisaTxtInput = ({ onChangeText, value }) => {
  return (
    <View style={styles.paisaView}>
      <Image source={AllIcons.paisa} style={styles.paisaImg} />
      <TextInput keyboardType={keyBoardType.numeric} value={value} onChangeText={onChangeText} style={styles.paisaInput} />
    </View>
  )
}
const styles = StyleSheet.create({
  denoBtn: {
    position: 'absolute', bottom: 160, marginLeft: "7%", width: "86%",

    height: 45,
    paddingVertical: 0,
    backgroundColor: colors.white,
    borderColor: colors.white,
    borderRadius: 50,
  },
  pricetxt: {
    fontSize: 15,
    marginLeft: 10
  },
  submitBtn: {
    position: 'absolute', bottom: 80, marginLeft: "7%", width: "86%",

    height: 45,
    paddingVertical: 0,
    backgroundColor: colors.buttonBlue,
    borderColor: colors.buttonBlue,
    borderRadius: 50,

  },
  btnTxt: {
    fontSize: 18,
    color: colors.white,
    fontWeight: '600'
  },
  denoTxt: {
    fontSize: 18,
    color: colors.black,
    fontWeight: '600'
  },
  mainContainer: {
    paddingHorizontal: "5%",
    paddingVertical: 10,
    marginTop: 20,

  },
  wallet: {
    fontSize: 17,
    color: colors.white, marginTop: 25, fontWeight: '300'
  },
  price: {
    fontSize: 40,
    fontWeight: '500',
    color: colors.white, marginLeft: 10
  },
  enterText: {
    fontSize: 14,
    color: colors.white,
    marginTop: 20, marginBottom: 10,
  },
  paisaView: {
    backgroundColor: colors.white,
    flexDirection: 'row', alignItems: 'center',
    borderRadius: 10,
    paddingHorizontal: 10
  },
  paisaImg: {
    height: 20, width: 20, resizeMode: 'contain' , marginTop : 5
  },
  paisaInput: {
    minWidth: 180,

    maxWidth: Dimensions.get('window').width - 100,
    paddingLeft: 10,
    fontSize: 24,
    fontWeight: 'bold', color: colors.black
  },
  transectionView: {
    flexDirection: 'row', alignItems: 'center', flexWrap: 'wrap'
  },
  icon: {
    height: 15, width: 15, resizeMode: 'contain'
  }
})