import { View, Text, StyleSheet, Dimensions, Image, TouchableOpacity, Alert } from 'react-native'
import React from 'react'
import { SafeAreaView } from 'react-native-safe-area-context';
import AuthFrame from '../../authScreens/AuthFrame';
import CustomBack from '../../common/CustomBack/CustomBack';
import QRCode from 'react-native-qrcode-svg';
import { AllIcons, colors } from '../../assets/common/Common';
import { Rtext } from '../../common/Rtext';
import { CusButtom } from '../../common/CusButtom';
const { height, width } = Dimensions.get('window');
export default function YourQrCode() {
    let sendObject = {
        name: "jay shah",
        userId: "uid1234",
        askedMoney: " props?.route?.params?.money",
        notesAndCount: "props?.route?.params?.currencyNotes"
    }
    return (
        <SafeAreaView>
            <AuthFrame >
                <CustomBack name='Your QR Code' />
                <View style={styles.qrView}>
                    <QRCode
                        size={width - 80}
                        value={JSON.stringify(sendObject)}
                    />
                </View>
                <TouchableOpacity onPress={() => { Alert.alert("More to come", "This screens needs to be design soon") }} style={styles.walletView}>
                    <Rtext style={styles.walletTxt}>Wallet ID : #234454454RET22</Rtext>
                    <Image style={styles.walletCopyImage} source={AllIcons.copy} />
                </TouchableOpacity>

                <View style={styles.btnView}>
                    <CusButtom onpress={() => { Alert.alert("More to come", "This screens needs to be design soon") }} BTNstyle={styles.btn} textStyle={styles.txt} ImgStyle={styles.icon} source={AllIcons.download} text={"Download QR"} />
                    <CusButtom onpress={() => { Alert.alert("More to come", "This screens needs to be design soon") }} BTNstyle={styles.btn} textStyle={styles.txt} ImgStyle={styles.icon} source={AllIcons.share} text={"Share QR Code"} />
                </View>
            </AuthFrame>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    qrView: {
        marginHorizontal: 20,
        backgroundColor: colors.white,
        paddingVertical: 20,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
        marginTop: 40
    },
    txt: {
        fontSize: 12,
        color: colors.black,
        marginLeft: 10,
        fontWeight: 'bold', marginRight: 5
    },
    walletView: {
        flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginTop: 20
    },
    walletTxt: {
        fontSize: 18,
        color: colors.white
    },
    walletCopyImage: {
        marginLeft: 10,
        height: 15, width: 15, resizeMode: 'contain'
    },
    btnView: {
        flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingHorizontal: 20, marginTop: 10
    },
    icon: {
        height: 20, width: 20, resizeMode: "contain"
    },
    btn: {
        paddingHorizontal: 10,
        backgroundColor: colors.white
    }
})