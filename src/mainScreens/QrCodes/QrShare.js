import { View, Text, StyleSheet, Image, Dimensions } from 'react-native'
import React from 'react'
import { SafeAreaView } from 'react-native-safe-area-context';
import { Rtext } from '../../common/Rtext';
import AuthFrame from '../../authScreens/AuthFrame';
import CustomBack from '../../common/CustomBack/CustomBack';
import { AllIcons, colors } from '../../assets/common/Common';
import QRCode from 'react-native-qrcode-svg';
import { CusButtom } from '../../common/CusButtom';
import { LeftHeadingTxt } from '../TabScreens/Home';

const { height , width} = Dimensions.get('window')
export default function QrShare(props) {
  let sendObject = {
    name: "jay shah",
    userId: "uid1234",
    askedMoney: " props?.route?.params?.money",
    notesAndCount: "props?.route?.params?.currencyNotes"
  }
  return (
    <SafeAreaView>
      <AuthFrame>
        <CustomBack name='Back' />
        <Rtext style={styles.shareTxt} >Please share this QR code with the Receiver</Rtext>
        <View style={styles.qrViewOverLapView}>
          <View style={styles.qrView}>
            <QRCode
              size={width -80}
              value={JSON.stringify(sendObject)}
            />
          </View>
          <LeftHeadingTxt headingStyle={styles.amountTxt} name='Amount selected' />
          <View>
            <View style={styles.selecetedMoneyView}>
              <Image style={styles.paisaBigImg} source={AllIcons.paisa} />
              <Rtext style={styles.selectedMoneyTxt}>{"500.50"}</Rtext>
            </View>
          </View>
          <CusButtom onpress={() => props.navigation.navigate("YourQrCode")} textStyle={styles.btnTxt} text={"Return Home"} BTNstyle={styles.submitBtn} />
        </View>
      </AuthFrame>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  shareTxt: {
    fontSize: 22, color: colors.white, paddingHorizontal: "7%", textAlign: 'center', marginTop: 30
  },
  qrViewOverLapView: {
    backgroundColor: colors.white, height: 1000, marginTop: 160
  },
  paisaBigImg: {
    height: 45, width: 45, resizeMode: 'contain'
  },
  selectedMoneyTxt: {
    fontSize: 45, marginRight: 60, fontWeight: 'bold', marginLeft: 10, color: colors.black
  },
  qrView: {
    // marginHorizontal: 20,
    backgroundColor: colors.white,
    // padding: 15,
    width : width -40,
    paddingVertical : 20,
    alignSelf :'center',
    marginTop: -120,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    shadowColor: 'rgba(0, 0, 0, 1)',
    shadowOpacity: 0.3,
    backgroundColor: colors.white,
    elevation: 5,
    shadowRadius: 15,
    shadowOffset: { width: 4, height: 4 },
  },
  amountTxt: {
    fontSize: 15,
    color: colors.black,
    marginLeft: 20,
    marginTop: 25,
    marginBottom: 10
  },
  selecetedMoneyView: {
    height: 100,
    marginHorizontal: 20,
    borderColor: 'rgba(0, 0, 0, 0.05)',
    borderWidth: 1,
    borderRadius: 10,
    flexDirection: 'row', alignItems: 'center', justifyContent: 'center',
    shadowColor: 'rgba(0, 0, 0, 1)',
    shadowOpacity: 0.3,
    backgroundColor: colors.white,
    elevation: 5,
    shadowRadius: 15,
    shadowOffset: { width: 4, height: 4 },

  },
  btnTxt: {
    fontSize: 18,
    color: colors.white,
    fontWeight: '600'
  },

  submitBtn: {
    marginHorizontal: 30,
    height: 45,
    paddingVertical: 0,
    backgroundColor: colors.buttonBlue,
    borderColor: colors.buttonBlue,
    borderRadius: 50,
    marginTop: 25
  },
})