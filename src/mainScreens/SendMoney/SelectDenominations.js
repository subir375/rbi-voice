import { View, Text, TouchableOpacity, Image, Dimensions, Alert, StyleSheet } from 'react-native'
import React, { useState } from 'react'
import { SafeAreaView } from 'react-native-safe-area-context';
import AuthFrame from '../../authScreens/AuthFrame';
import CustomBack from '../../common/CustomBack/CustomBack';
import { Rtext } from '../../common/Rtext';
import { AllIcons, colors, moneyArray } from '../../assets/common/Common';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { LeftHeadingTxt } from '../TabScreens/Home';
import { CusButtom } from '../../common/CusButtom';
const { width, height } = Dimensions.get('window')
export default function SelectDenominations(props) {
    const [selectedNote, setSelectedNote] = useState([
        {
            value: 2.00,
            selectedVal: 0,
        },
        {
            value: 5.00,
            selectedVal: 0,
        },
        {
            value: 10.00,
            selectedVal: 0,
        },
        {
            value: 20.00,
            selectedVal: 0,
        },
        {
            value: 50.00,
            selectedVal: 0,
        },
        {
            value: 100.00,
            selectedVal: 0,
        }
        ,
        {
            value: 200.00,
            selectedVal: 0,
        },
        {
            value: 500.00,
            selectedVal: 0,
        },
        {
            value: 2000.00,
            selectedVal: 0,
        }
    ])



    const returnVal = (val) => {


        let data = selectedNote.filter((item) => item.value === val)


        return data[0].selectedVal
    }


    const AddOrRemoveNote = (val, type = "", selectedValue = 0) => {
        if (type === "minus") {
            if (selectedValue === 0) {
                Alert.alert("Cant remove note the value is 0 currently. please add instead")
                return
            }
        }
        let newSelectedArr = [...selectedNote];
        let updatedValue = newSelectedArr.map((item) => {
            if (item.value === val) {
                return {
                    value: item.value,
                    selectedVal: item.selectedVal + (type === "plus" ? + 1 : -1),
                }
            } else {
                return item
            }
        })
        if(totalMoneyCalChcek(updatedValue) <= props.route.params.money  ){
            console.log("totalMoneyCalChcek(updatedValue)",totalMoneyCalChcek(updatedValue))
            setSelectedNote(updatedValue)
        }else{
            Alert.alert("Wrong selection" , "Please select the notes for rupees " +props.route.params.money )
        }

    }


    
    const totalMoneyCalChcek = (selectNotes) => {
        let totalMoney = 0.00;

        for (let index = 0; index < selectNotes.length; index++) {
            const element = selectNotes[index];
            totalMoney = element.selectedVal * element.value + totalMoney
        }

        return parseFloat(totalMoney.toFixed(2));
    }

    const totalMoneyCal = () => {
        let totalMoney = 0.00;

        for (let index = 0; index < selectedNote.length; index++) {
            const element = selectedNote[index];
            totalMoney = element.selectedVal * element.value + totalMoney
        }

        return parseFloat(totalMoney.toFixed(2));
    }

    return (
        <SafeAreaView>
            <AuthFrame>
                <CustomBack name='Select Denominations' />
                <View style={styles.moneyAvaView}>
                        <Rtext style={styles.avaTxt}>Available Balance :</Rtext>
                        <Image style={styles.paisaImg} source={AllIcons.paisa} />
                        <Rtext style={styles.moneyTxt}>500.00</Rtext>
                    </View>
                    <Rtext style={styles.denoTxt}>Available Denominations</Rtext>
                <KeyboardAwareScrollView>
                 
                    <View style={styles.overLapedView}>
                        <View style={styles.moneyContainer}>
                            {
                                moneyArray.map((item, index) => (
                                    <View style={{ width: width / 3 - 34, alignItems: 'center', justifyContent: 'center', marginBottom: 20 }}>
                                        <Image style={{ height: 130, width: "80%", resizeMode: 'stretch' }} source={item.image} />
                                        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, }}>
                                            <Image style={{ height: 14, width: 14, resizeMode: 'contain' }} source={AllIcons.paisa} />
                                            <Rtext style={{ fontSize: 15, color: colors.black, fontWeight: "bold" }}>{item.value}</Rtext>
                                        </View>
                                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                            <TouchableOpacity onPress={() => AddOrRemoveNote(item.value, "minus", returnVal(item.value))} >
                                                <Image style={{ height: 35, width: 35, resizeMode: 'contain' }} source={require('../../assets/moneyicons/min.png')} />
                                            </TouchableOpacity>
                                            <Rtext style={{ width: 30, textAlign: 'center', fontSize: 16 }}> {returnVal(item.value)} </Rtext>
                                            <TouchableOpacity onPress={() => AddOrRemoveNote(item.value, "plus")}>
                                                <Image style={{ height: 35, width: 35, resizeMode: 'contain' }} source={require('../../assets/moneyicons/pl.png')} />
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                ))
                            }
                        </View>
                        <LeftHeadingTxt headingStyle={styles.amountTxt} name='Amount selected' />
                        <View>
                            <View style={styles.selecetedMoneyView}>
                                <Image style={styles.paisaBigImg} source={AllIcons.paisa} />
                                <Rtext style={styles.selectedMoneyTxt}>{totalMoneyCal()}</Rtext>
                            </View>
                        </View>
                        <CusButtom onpress={() => {
                            if (props.route.params.money === parseFloat(totalMoneyCal())) {
                                props.navigation.navigate('SuccessNotes' , {selectedNote , totalMoney :totalMoneyCal()})
                            }else{
                                Alert.alert("Please Select" , "Select the notes equal to ₹" + props.route.params.money )
                            }
                        }} textStyle={styles.btnTxt} text={"Select Denominations"} BTNstyle={styles.submitBtn} />
                    </View>
                </KeyboardAwareScrollView>
            </AuthFrame>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    moneyContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        flexWrap: 'wrap',
        alignSelf: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 10,
        backgroundColor: colors.white,
        marginHorizontal: 20,
        borderRadius: 10,
        marginTop: -120,

        shadowColor: 'rgba(0, 0, 0, 1)',
        shadowOpacity: 0.3,
        elevation: 5,
        shadowRadius: 15,
        shadowOffset: { width: 4, height: 4 },
    },
    btnTxt: {
        fontSize: 18,
        color: colors.white,
        fontWeight: '600'
    },

    submitBtn: {
        marginHorizontal: 30,
        height: 45,
        paddingVertical: 0,
        backgroundColor: colors.buttonBlue,
        borderColor: colors.buttonBlue,
        borderRadius: 50,
        marginTop: 25
        , marginBottom: 80
    },
    overLapedView: {
        backgroundColor: colors.white,
        marginTop: 120
    },
    moneyAvaView: {
        flexDirection: 'row', alignItems: 'center', paddingHorizontal: 20, marginVertical: 20
    },
    avaTxt: {
        fontSize: 18, color: colors.white
    },
    paisaImg: {
        height: 12, width: 12, marginTop: 10, resizeMode: 'contain', tintColor: colors.white, marginHorizontal: 10
    },
    moneyTxt: {
        fontSize: 20, fontWeight: 'bold', color: colors.white
    },
    denoTxt: {
        fontSize: 18, color: colors.white, marginLeft: 20, marginBottom: 10
    },
    selecetedMoneyView: {
        height: 100,
        marginHorizontal: 20,
        borderColor: 'rgba(0, 0, 0, 0.05)',
        borderWidth: 1,
        borderRadius: 10,
        flexDirection: 'row', alignItems: 'center', justifyContent: 'center',
        shadowColor: 'rgba(0, 0, 0, 1)',
        shadowOpacity: 0.3,
        backgroundColor: colors.white,
        elevation: 5,
        shadowRadius: 15,
        shadowOffset: { width: 4, height: 4 },

    },
    amountTxt: {
        fontSize: 18,
        color: colors.black,
        marginLeft: 20,
        marginTop: 25,
        marginBottom: 10
    },
    selectedMoneyTxt: {
        fontSize: 45, marginRight: 60, fontWeight: 'bold', marginLeft: 10, color: colors.black
    },
    paisaBigImg: {
        height: 45, width: 45, resizeMode: 'contain'
    }
})