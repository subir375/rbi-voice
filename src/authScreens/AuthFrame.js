import React from 'react'
import { View, Text, StyleSheet,TouchableOpacity, SafeAreaView, TouchableWithoutFeedback, Dimensions, Keyboard, ImageBackground, Pressable, Alert } from 'react-native'
// import {  } from 'react-native-gesture-handler';
import {  } from 'react-native-safe-area-context';
import { colors } from '../assets/common/Common'
const { height, width } = Dimensions.get('window');

export default function AuthFrame({ children, onPress=()=> {}, imageUri, containerOpacity = false }) {
    return (
        <SafeAreaView style={containerOpacity ? styles.containerOpacity : styles.container}>
            <ImageBackground imageStyle={styles.image} source={require('../assets/image/appBackground.png')} style={styles.background} >
                <TouchableOpacity style={styles.background} activeOpacity={1} onPress={() => {
                    Keyboard.dismiss();
                    // alert("oppp")
                    onPress();
                }}
                >
                    <>
                    {
                        children
                    }
                    </>
                </TouchableOpacity>
            </ImageBackground>
        </SafeAreaView>
    )
}



const styles = StyleSheet.create({
    container: {
        padding: 10,
        height: height,
        width: width,
        backgroundColor: colors.white,
        alignItems: 'center',
        justifyContent: 'center',
    },

    containerOpacity: {
        padding: 10,
        height: height,
        width: width,
        backgroundColor: colors.white,
        alignItems: 'center',
        justifyContent: 'center',
        opacity: 0.67,
        backgroundColor: 'black',
    },
    img: {
        marginTop: 20,
        width: width - 30,
        height: 250,
        resizeMode: 'contain'
    },
    background: {
        height: height,
        width: width,
    },
    image: {
        height: height,
        width: width,
    }
})