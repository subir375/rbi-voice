import React, {useState} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  Dimensions,
  Alert,
  PermissionsAndroid,
} from 'react-native';
import {Rtext} from '../common/Rtext';
import {Ainput} from '../common/Ainput';
import AuthFrame from './AuthFrame';
import {
  AllIcons,
  colors,
  keyBoardType,
  startAudioRecord,
} from '../assets/common/Common';
import {CusButtom} from '../common/CusButtom';
import {useNavigation} from '@react-navigation/native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {TextInput} from 'react-native-gesture-handler';
import {SafeAreaView} from 'react-native-safe-area-context';
import {NewRecorder} from '../helperClasses/Recoder';
import {useDispatch, useSelector} from 'react-redux';
import {
  loaderOff,
  loaderOn,
  saveUserData,
  textToSpeech,
} from '../Store/AuthReducer';
import {useEffect} from 'react';
import {userStackChange} from '../Store/AuthReducer';
import {request} from '../utility/common';
import Pin from '../mainScreens/Pin/Pin';
import {FingerPrint} from '../component/FingerPrint';
import {NewTextToSpeech} from '../component/TextToSpeech';
import SpeechToText from '../component/SpeechToText';
const {height, width} = Dimensions.get('window');

export default function PhoneNumber() {
  const language = useSelector(store => store?.auth?.language);
  const LanguageCheck = () => {
    return language === 'hindi';
  };
  const dispatch = useDispatch();
  const [showPinModel, setPinModel] = useState(false);
  const [userName, setUserName] = useState('');
  const [Password, setPassword] = useState('');
  const [voiceText, setvoiceText] = useState('');
  const [speechToText, partial, results, speechError, _destroyRecognizer] =
    SpeechToText();

  const getUserName = () => {
    NewTextToSpeech.readText('For login please told us your nickname only');
    setTimeout(() => {
      // setIsCalled(false);
      speechToText();
    }, 2800);
  };
  const _requestRecordAudioPermission = async () => {
    return new Promise((resolve, reject) => {
      try {
        const granted = PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
          {
            title: 'Microphone Permissions',
            message:
              'rbiVoice need to access your microphone for communications.',
            buttonNeutral: 'Ask Me Later',
            buttonNegative: 'cancel',
            buttonPositive: 'ok',
          },
        );
        if (PermissionsAndroid.RESULTS.GRANTED === 'granted') {
          resolve(true);
        } else {
          resolve(false);
        }
      } catch (error) {
        resolve(false);
      }
    });
  };

  useEffect(() => {
    _requestRecordAudioPermission().then(res => {
      getUserName();
    });
  }, []);
  useEffect(() => {
    console.log('results?', results);
    // console.log('error', speechError);
    if (speechError.length > 0) {
      NewTextToSpeech.readText('Audio not detected please try again.');
    } else if (results.length > 0) {
      setUserName(results[0]);
      NewTextToSpeech.readText(
        LanguageCheck()
          ? 'अपनी अंगुली को फ़िंगरप्रिंट स्कैनर पर रखें'
          : 'Place your finger on the fingerprint scanner',
      );
      FingerPrint().then(res => {
        if (res) {
          login(results[0]);
        }
      });
      _destroyRecognizer;
    }
  }, [results, speechError]);
  const login = nickName => {
    if (nickName === '') {
      Alert.alert('Wrong name.', userName, nickName);
      return;
    }
    dispatch(loaderOn());

    request('post', 'http://events.respark.iitm.ac.in:5000/rp_bank_api', {
      action: 'details',
      nick_name: nickName === '' ? userName : nickName,
    })
      .then(resp => {
        dispatch(loaderOff());
        console.log('resp.data =====>>>>', resp.data);
        if (resp.data === 'None') {
          NewTextToSpeech.readText('you have entered wrong Nick name');
          return;
        } else {
          dispatch(saveUserData(resp.data));
          dispatch(userStackChange());
          NewTextToSpeech.readText('you are  successfully logged in');
        }
      })
      .catch(error => {
        dispatch(loaderOff());
        NewTextToSpeech.readText('you have entered wrong Nick name');
      });
  };
  return (
    <SafeAreaView>
      <AuthFrame onPress={() => getUserName()}>
        <View style={{height}}>
          <KeyboardAwareScrollView style={{flex: 1}}>
            <View style={{height: height}}>
              <View style={styles.logoContainer}>
                <Image style={styles.icon} source={AllIcons.logoIcon} />
                <Rtext style={styles.headingTxt}>
                  {LanguageCheck() ? 'आरबी बैंक' : 'RB Bank'}
                </Rtext>
                <Rtext style={styles.heading2}>
                  {LanguageCheck()
                    ? 'सरकारी भुगतान द्वारा संचालित'
                    : 'Powered by Government Payments'}
                </Rtext>
                <View style={styles.txtiView}>
                  <TextInput
                    placeholder={LanguageCheck() ? 'निक नाम' : 'Nick name'}
                    value={userName}
                    onChangeText={val => {
                      setUserName(val);
                    }}
                    style={styles.txtInput}
                  />
                  {/* <TextInput placeholder={"Password"} keyboardType={keyBoardType.numeric} value={Password} onChangeText={(val) => {
                                        setPassword(val)
                                    }} style={styles.txtInput} /> */}
                </View>
                <CusButtom
                  onpress={() => {
                    // userName !== '' && setPinModel(true);
                    NewTextToSpeech.readText(
                      LanguageCheck()
                        ? 'अपनी अंगुली को फ़िंगरप्रिंट स्कैनर पर रखें'
                        : 'Place your finger on the fingereprint scanner',
                    );
                    FingerPrint().then(res => {
                      if (res) {
                        console.log('us', userName);
                        login();
                      }
                    });
                  }}
                  textStyle={styles.btnTxt}
                  text={LanguageCheck() ? 'लॉग इन करें' : 'Login'}
                  BTNstyle={styles.generateOtpBtn}
                />
              </View>
              {/* <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  maxHeight: height * 0.2,
                  maxWidth: width * 0.6,
                  padding: 10,
                }}>
                {voiceText.length>0 && (
                  <Rtext style={styles.headingTxt}>{voiceText}</Rtext>
                )}
              </View> */}

              <View
                style={{marginLeft: '5%', flex: 1, justifyContent: 'flex-end'}}>
                <View
                  style={{
                    width: '90%',
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    marginBottom: 10,
                  }}>
                  <View
                    style={{
                      width: (width * 0.9 - 165) / 2 - 10,
                      height: 1,
                      backgroundColor: colors.white,
                    }}
                  />
                  <Rtext
                    style={{
                      width: 165,
                      fontSize: 13,
                      color: colors.white,
                      marginHorizontal: 10,
                      textAlign: 'center',
                    }}>
                    {LanguageCheck()
                      ? 'आरबी बैंक क्यों चुनें?'
                      : 'Why Choose RB BANK ?'}
                  </Rtext>
                  <View
                    style={{
                      width: (width * 0.9 - 165) / 2 - 10,
                      height: 1,
                      backgroundColor: colors.white,
                    }}
                  />
                </View>
                <Rtext style={styles.desc}>
                  {LanguageCheck()
                    ? 'उपयोग में आसान, कुछ ही चरणों में अपना वॉलेट बनाएं'
                    : 'Easy To Use, Create Your Wallet In Just Few Steps '}
                </Rtext>
                <Rtext style={styles.desc}>
                  {LanguageCheck()
                    ? 'ऑनलाइन और ऑफलाइन दोनों मोड में काम करता है'
                    : 'Works Both In Online And Offline Mode'}{' '}
                </Rtext>
                <Rtext style={styles.desc}>
                  {LanguageCheck()
                    ? 'बदलते भारत का हिस्सा बनें'
                    : 'Be The Part Of Changing INDIA'}{' '}
                </Rtext>
                <Rtext style={styles.desc}>
                  {LanguageCheck()
                    ? 'आरबीआई ने डिजिटल करेंसी ऐप जारी किया'
                    : 'RBI Issued Digital Currency App'}{' '}
                </Rtext>
              </View>
              <View
                style={{
                  height: 1,
                  backgroundColor: colors.white,
                  width: '80%',
                  alignSelf: 'center',
                  marginTop: 10,
                }}
              />
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  paddingHorizontal: '5%',
                  justifyContent: 'space-between',
                  paddingVertical: 10,
                }}>
                <Image style={styles.imageFlag} source={AllIcons.indianFlag} />
                <Image style={styles.smallLogo} source={AllIcons.logoIcon} />
                <View>
                  <Rtext style={{...styles.headingTxt, marginTop: 0}}>
                    {LanguageCheck() ? 'आरबी बैंक' : 'RB Bank'}
                  </Rtext>
                  <Rtext style={styles.heading2}>
                    {LanguageCheck()
                      ? 'सरकारी भुगतान द्वारा संचालित'
                      : 'Powered by Government Payments'}
                  </Rtext>
                </View>
              </View>
            </View>
          </KeyboardAwareScrollView>
        </View>
      </AuthFrame>
      {userName !== '' && showPinModel && (
        <Pin setPinModel={setPinModel} method={login} />
      )}
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  Phone: {
    marginTop: 20,
  },
  icon: {
    height: 140,
    width: 130,
    resizeMode: 'contain',
    alignItems: 'center',
  },
  logoContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 40,
  },
  headingTxt: {
    fontSize: 25,
    marginTop: 12,
    color: colors.white,
    fontWeight: 'bold',
    // textAlign: 'center'
  },
  heading2: {
    fontSize: 12,
    color: colors.white,
    marginBottom: 20,
  },
  walletTxt: {
    fontSize: 20,
    color: colors.white,
    fontWeight: '600',
    marginTop: 30,
  },
  filltxt: {
    fontSize: 14,
    marginTop: 8,
    color: colors.white,
    marginHorizontal: '5%',
  },
  adartxt: {
    fontSize: 14,
    marginTop: 25,
    marginBottom: 10,
    color: colors.white,
  },
  txtInput: {
    height: 45,
    padding: 10,
    marginTop: 40,
    backgroundColor: colors.white,
    borderRadius: 5,
    color: colors.black,
  },
  txtiView: {
    width: '90%',
  },
  generateOtpBtn: {
    width: '90%',
    height: 45,
    paddingVertical: 0,
    backgroundColor: colors.buttonBlue,
    borderColor: colors.buttonBlue,
    borderRadius: 50,
    marginTop: 25,
  },
  btnTxt: {
    fontSize: 18,
    color: colors.white,
    fontWeight: '600',
  },
  desc: {
    fontSize: 12,
    color: colors.white,
    paddingTop: 5,
    textAlign: 'center',
  },
  imageFlag: {
    height: 45,
    width: 80,
  },
  smallLogo: {
    height: 45,
    width: 45,
    resizeMode: 'contain',
  },
});
