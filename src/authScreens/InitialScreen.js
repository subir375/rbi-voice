import {
  View,
  Text,
  StyleSheet,
  Image,
  PermissionsAndroid,
  Dimensions,
} from 'react-native';
import React, {useEffect} from 'react';
import AuthFrame from './AuthFrame';
import {CusButtom} from '../common/CusButtom';
import {AllIcons, colors, startAudioRecord} from '../assets/common/Common';
import {Rtext} from '../common/Rtext';
import {useNavigation} from '@react-navigation/native';
import {useDispatch, useSelector} from 'react-redux';
import {textToSpeech} from '../Store/AuthReducer';
import {useState} from 'react';
import TextToSpeech, {NewTextToSpeech} from '../component/TextToSpeech';
import SpeechToText from '../component/SpeechToText';
const {height, width} = Dimensions.get('window');
export default function InitialScreen() {
  const dispatch = useDispatch();
  const [isCalled, setIsCalled] = useState(false);
  const [voiceText, setvoiceText] = useState('');
  const [speechText, setspeechText] = useState('');

  const language = useSelector(store => store?.auth?.language);
  const [speechToText, partial, results, speechError, _destroyRecognizer] =
    SpeechToText();

  const LanguageCheck = () => {
    return language === 'hindi';
  };
  const navigation = useNavigation();

  const _requestRecordAudioPermission = async () => {
    try {
      const granted = PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
        {
          title: 'Microphone Permissions',
          message:
            'rbiVoice need to access your microphone for communications.',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'cancel',
          buttonPositive: 'ok',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        return true;
      } else {
        return false;
      }
    } catch (error) {
      return false;
    }
  };

  const OptionsForUser = () => {
    if (isCalled) {
      return;
    }
    setIsCalled(true);
    NewTextToSpeech.readText(
      'Please select one option , for login say login,  for registration say registration',
    );
    setTimeout(() => {
      setIsCalled(false);
      speechToText();
    }, 6000);
  };
  useEffect(() => {
    _requestRecordAudioPermission();
  }, []);

  useEffect(() => {
    // console.log('results', results);
    // console.log('error', speechError);
    if (speechError.length > 0 || speechError?.message?.length > 0) {
      NewTextToSpeech.readText('Audio not detected please try again.');
    } else if (results.length > 0) {
      if (results[0].toLocaleLowerCase().includes('login')) {
        setIsCalled(false);
        navigation.navigate('login');
      } else if (results[0].toLocaleLowerCase().includes('registration')) {
        navigation.navigate('registration');
        setIsCalled(false);
        setvoiceText('');
      } else if (results[0].toLocaleLowerCase().includes('stop')) {
        setIsCalled(false);
        NewTextToSpeech.readText(
          'Speech recognition is stop to start again tap on screen',
        );
      } else {
        NewTextToSpeech.readText(results[0] + 'is a wrong option.');
        setIsCalled(false);
      }
      _destroyRecognizer();
    }
  }, [results, speechError]);

  return (
    <View>
      <AuthFrame
        onPress={() => {
          setIsCalled(true);
          !isCalled && OptionsForUser();
        }}>
        <View style={styles.container}>
          <Image style={styles.icon} source={AllIcons.logoIcon} />
          <Rtext style={styles.headingTxt}>
            {LanguageCheck() ? 'आरबी बैंक' : 'RB Bank'}
          </Rtext>
          <CusButtom
            onpress={() => {
              navigation.navigate('login');
              // speechToText(); // this is for speech to text using model
            }}
            textStyle={styles.btnTxt}
            text={LanguageCheck() ? 'लॉग इन करें' : 'Login'}
            BTNstyle={styles.generateOtpBtn}
          />
          <CusButtom
            onpress={() => {
              navigation.navigate('registration');
            }}
            textStyle={styles.btnTxt}
            text={LanguageCheck() ? 'पंजीकरण' : 'Registration'}
            BTNstyle={styles.generateOtpBtn}
          />
          {/* <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              maxHeight: height * 0.2,
              maxWidth: width * 0.6,
            }}>
            {isCalled && <Rtext style={styles.headingTxt}>{voiceText}</Rtext>}
          </View> */}
        </View>
      </AuthFrame>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  icon: {
    height: 140,
    width: 130,
    resizeMode: 'contain',
    alignItems: 'center',
  },
  headingTxt: {
    fontSize: 25,
    marginTop: 12,
    color: colors.white,
    fontWeight: 'bold',
    // textAlign: 'center'
  },
  btnTxt: {
    fontSize: 18,
    color: colors.white,
    fontWeight: '600',
  },
  generateOtpBtn: {
    width: '90%',
    height: 45,
    paddingVertical: 0,
    backgroundColor: colors.buttonBlue,
    borderColor: colors.buttonBlue,
    borderRadius: 50,
    marginTop: 25,
  },
  headingTxt: {
    fontSize: 25,
    marginTop: 12,
    color: colors.white,
    fontWeight: 'bold',
    // textAlign: 'center'
  },
});
