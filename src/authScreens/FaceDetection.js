import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  Button,
} from 'react-native';
import {Camera, CameraType} from 'expo-camera';
import * as FaceDetector from 'expo-face-detector';
import {useRef} from 'react';
import FaceBox from '../component/FaceBox';
import {AllIcons} from '../assets/common/Common';
const cameraRef = useRef < Camera > null;
const FaceDetectionFunction = () => {
  const [type, setType] = useState(CameraType.front);
  const [permission, requestPermission] = Camera.useCameraPermissions();
  const [bounds, setBounds] = useState({
    origin: {x: 0, y: 0},
    size: {height: 0, width: 0},
  });
  const [features, setFeatures] = useState({
    BOTTOM_MOUTH: {x: 0, y: 0},
    LEFT_CHEEK: {x: 0, y: 0},
    LEFT_EAR: {x: 0, y: 0},
    LEFT_EYE: {x: 0, y: 0},
    LEFT_MOUTH: {x: 0, y: 0},
    NOSE_BASE: {x: 0, y: 0},
    RIGHT_CHEEK: {x: 0, y: 0},
    RIGHT_EAR: {x: 0, y: 0},
    RIGHT_EYE: {x: 0, y: 0},
    RIGHT_MOUTH: {x: 0, y: 0},
    faceID: 0,
    leftEyeOpenProbability: 0,
    rightEyeOpenProbability: 0,
    rollAngle: 0,
    smilingProbability: 0,
    yawAngle: 0,
  });

  // This is a constant that sets the size of the face features boxes.
  const FEATURE_SIZE = {height: 25, width: 25};
  if (!permission) {
    // Camera permissions are still loading
    return <View />;
  }

  if (!permission.granted) {
    // Camera permissions are not granted yet
    return (
      <View style={styles.container}>
        <Text style={{textAlign: 'center'}}>
          We need your permission to show the camera
        </Text>
        <Button onPress={requestPermission} title="grant permission" />
      </View>
    );
  }


  // This function toggles the camera type between front-facing and back-facing.
  function toggleCameraType() {
    setType(current =>
      current === CameraType.back ? CameraType.front : CameraType.back,
    );
  }
  const handleFacesDetected = ({faces}) => {
    if (faces[0] != undefined) {
      console.log('faces[0] ', faces[0]);
      setBounds(faces[0].bounds);
      setFeatures(faces[0]);
    }
  };
  return (
    <View style={styles.container}>
      <Camera
        style={styles.camera}
        onFacesDetected={handleFacesDetected}
        type={type}
        faceDetectorSettings={{
          mode: FaceDetector.FaceDetectorMode.accurate,
          detectLandmarks: FaceDetector.FaceDetectorLandmarks.all,
          runClassifications: FaceDetector.FaceDetectorClassifications.all,
          minDetectionInterval: 100,
          tracking: true,
        }}>
        <View style={styles.buttonContainer}>
          <TouchableOpacity style={styles.button} onPress={() => {}}>
            <Image
              source={AllIcons.camera2}
              style={{height: 40, width: 40, tintColor: '#FFFF'}}
            />
          </TouchableOpacity>
          <TouchableOpacity style={styles.button} onPress={toggleCameraType}>
            <Image
              source={AllIcons.flip}
              style={{height: 40, width: 40, tintColor: '#FFFF'}}
            />
          </TouchableOpacity>
        </View>
      </Camera>
      <FaceBox
        origin={bounds.origin}
        size={bounds.size}
        name={`Face ${features.faceID} Roll: ${Math.round(
          features.rollAngle,
        )}° Yaw: ${Math.round(features.yawAngle)}°`}
      />
      <FaceBox
        origin={features.LEFT_EYE}
        size={FEATURE_SIZE}
        name={`${Math.round(features.leftEyeOpenProbability * 100)}%`}
      />
      <FaceBox
        origin={features.RIGHT_EYE}
        size={FEATURE_SIZE}
        name={`${Math.round(features.rightEyeOpenProbability * 100)}%`}
      />
      <FaceBox
        origin={features.BOTTOM_MOUTH}
        size={FEATURE_SIZE}
        name={`${Math.round(features.smilingProbability * 100)}%`}
      />
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  camera: {
    flex: 1,
  },
  buttonContainer: {
    flex: 1,
    // height:50,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    backgroundColor: 'black',
    alignItems: 'flex-end',
    alignSelf: 'flex-end',
    position: 'absolute',
    bottom: 5,
    zIndex: 1,
    padding: 20,
    // margin: 64,
  },
  button: {
    flex: 1,
    alignSelf: 'flex-end',
    alignItems: 'center',
  },
  text: {
    fontSize: 24,
    fontWeight: 'bold',
    color: 'white',
  },
});
export default FaceDetectionFunction;
