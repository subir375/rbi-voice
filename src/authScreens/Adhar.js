import React, {useState} from 'react';
import {View, Text, Image, StyleSheet, Dimensions, Alert} from 'react-native';
import {Rtext} from '../common/Rtext';
import {Ainput} from '../common/Ainput';
import AuthFrame from './AuthFrame';
import {
  AllIcons,
  baseUrlRb,
  colors,
  keyBoardType,
  startAudioRecord,
} from '../assets/common/Common';
import {CusButtom} from '../common/CusButtom';
import {useIsFocused, useNavigation} from '@react-navigation/native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {TextInput} from 'react-native-gesture-handler';
import {SafeAreaView} from 'react-native-safe-area-context';
import {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {loaderOff, loaderOn, textToSpeech} from '../Store/AuthReducer';
import {request} from '../utility/common';
import {FingerPrint} from '../component/FingerPrint';
const {height, width} = Dimensions.get('window');
import {NewTextToSpeech} from '../component/TextToSpeech';
import SpeechToText from '../component/SpeechToText';
export default function Adhar() {
  const language = useSelector(store => store?.auth?.language);
  const LanguageCheck = () => {
    return language === 'hindi';
  };
  const dispatch = useDispatch();
  const isfocus = useIsFocused();
  const [nickName, setNickName] = useState('');
  const [fullName, setFullName] = useState('');
  const [userName, setUserName] = useState('');
  const [pinNumber, setPinNumber] = useState('');
  const [mobile, setMobile] = useState('');
  const [upiId, setUpiId] = useState('');
  const navigation = useNavigation();
  const [adhar, setAdhar] = useState('');
  const [isCalled, setIsCalled] = useState(false);
  const [currentFiledIndex, setCurrentFiledIndex] = useState(0);
  const [speechToText, partial, results, speechError, _destroyRecognizer] =
    SpeechToText();

  useEffect(() => {
    getValues();
  }, []);
  useEffect(() => {
    console.log('results?', results);
    // console.log('error', speechError);
    if (speechError.length > 0) {
      NewTextToSpeech.readText(
        'Audio not detected please try again to continue please tap on the screen again.',
      );
    } else if (results.length > 0) {
      incrementIndex();
      if (currentFiledIndex === 0) {
        setNickName(results[0]);
      } else if (currentFiledIndex === 1) {
        setFullName(results[0]);
      } else if (currentFiledIndex === 2) {
        setUserName(results[0]);
      } else if (currentFiledIndex === 3) {
        setPinNumber(results[0]);
      } else if (currentFiledIndex === 4) {
        setMobile(results[0]);
      } else if (currentFiledIndex === 5) {
        setUpiId(results[0]);
      }

      _destroyRecognizer;
    }
  }, [results, speechError]);
  useEffect(() => {
    if (currentFiledIndex !== 0 && currentFiledIndex !== 6) {
      console.log('useEffect');
      getValues();
    } else if (currentFiledIndex === 6) {
      registerApi();
    }
  }, [currentFiledIndex]);
  const incrementIndex = () => {
    setCurrentFiledIndex(currentFiledIndex + 1);
  };

  const getValues = () => {
    if (!isfocus) {
      console.log('isfocus>', isfocus);
      return;
    } else {
      console.log('isfocus', isfocus);
    }
    if (currentFiledIndex === 0) {
      NewTextToSpeech.readText(
        LanguageCheck()
          ? 'कृपया हमें अपना उपनाम बत'
          : 'Please told us your nickname',
      );
      setTimeout(() => {
        speechToText();
      }, 2800);
    } else if (currentFiledIndex === 1) {
      NewTextToSpeech.readText(
        LanguageCheck()
          ? 'कृपया हमें अपना पूरा नाम बताएं'
          : 'Please told us your Full name',
      );
      setTimeout(() => {
        speechToText();
      }, 2800);
    } else if (currentFiledIndex === 2) {
      NewTextToSpeech.readText(
        LanguageCheck()
          ? 'कृपया हमें अपना उपयोगकर्ता नाम बताएं'
          : 'Please told us your Username',
      );
      setTimeout(() => {
        speechToText();
      }, 2800);
    } else if (currentFiledIndex === 3) {
      NewTextToSpeech.readText(
        LanguageCheck()
          ? 'कृपया हमें अपना पिन नंबर बताएं'
          : 'Please told us your Pin Number',
      );
      setTimeout(() => {
        speechToText();
      }, 2800);
    } else if (currentFiledIndex === 4) {
      NewTextToSpeech.readText(
        LanguageCheck()
          ? 'कृपया हमें अपना मोबाइल नंबर बताएं'
          : 'Please told us your Mobile Number',
      );
      setTimeout(() => {
        speechToText();
      }, 2800);
    } else if (currentFiledIndex === 5) {
      NewTextToSpeech.readText(
        LanguageCheck()
          ? 'कृपया हमें अपनी यूपीआई आईडी बताएं'
          : 'Please told us your u p i I d',
      );
      setTimeout(() => {
        speechToText();
      }, 2800);
    }
  };
  const registerApi = async () => {
    if (
      nickName === '' ||
      fullName === '' ||
      userName === '' ||
      userName === '' ||
      pinNumber === '' ||
      mobile === '' ||
      upiId === ''
    ) {
      Alert.alert('Wrong input', 'Please field can not be empty');
      NewTextToSpeech.readText(
        LanguageCheck()
          ? 'कृपया फ़ील्ड खाली नहीं हो सकता'
          : 'Please field can not be empty',
      );
      return;
    } else {
      NewTextToSpeech.readText(
        LanguageCheck() ? 'अपनी अंगुली को फ़िंगरप्रिंट स्कैनर पर रखें' : 'Place your finger on the fingereprint scanner',
      );
      FingerPrint().then(async res => {
        if (res) {
          dispatch(loaderOn());
          request('post', baseUrlRb, {
            action: 'register',
            nick_name: nickName,
            full_name: fullName,
            user_name: userName,
            pin_number: pinNumber,
            mob_number: mobile,
            upi_id: upiId,
          })
            .then(resp => {
              console.log('resp========>>>>', resp.data);
              dispatch(loaderOff());
              if (resp.data.status === 'failed') {
                NewTextToSpeech.readText(
                  LanguageCheck() ? 'कुछ गलत हो गया' : 'somthing went wrong',
                );
              } else {
                NewTextToSpeech.readText(
                  LanguageCheck()
                    ? 'आपका खाता सफलतापूर्वक बनाया गया'
                    : `your account created successfully`,
                );
                setTimeout(() => {
                  Alert.alert(
                    LanguageCheck()
                      ? 'आपका खाता सफलतापूर्वक बनाया गया'
                      : `your account created successfully`,
                  );
                  navigation.goBack();
                }, 2000);
              }
            })
            .catch(error => {
              console.log('error====>>>>', error);
              dispatch(loaderOff());
            });
        } else {
          console.log('not authenticated');
          dispatch(loaderOff());
          return;
        }
      });
    }
  };

  return (
    <AuthFrame
      onPress={() => {
        // setIsCalled(true);
        // !isCalled &&
        getValues();
      }}>
      <KeyboardAwareScrollView
        extraScrollHeight={100}
        enableOnAndroid={true}
        keyboardShouldPersistTaps="handled">
        <Image style={styles.icon} source={AllIcons.logoIcon} />
        <Rtext style={styles.headingTxt}>
          {LanguageCheck() ? 'आरबी बैंक' : 'RB Bank'}
        </Rtext>
        <Rtext style={styles.heading2}>
          {LanguageCheck()
            ? 'सरकारी भुगतान द्वारा संचालित'
            : 'Powered by Government Payments'}
        </Rtext>

        <View style={styles.txtiView}>
          <TextInput
            placeholder={LanguageCheck() ? 'उपनाम' : 'Nickname'}
            value={nickName}
            onChangeText={val => {
              setNickName(val);
            }}
            style={styles.txtInput}
          />
          <TextInput
            placeholder={LanguageCheck() ? 'पूरा नाम' : 'Fullname'}
            value={fullName}
            onChangeText={val => {
              setFullName(val);
              // setAdhar(val)
              //   }
            }}
            style={styles.txtInput}
          />
          <TextInput
            placeholder={LanguageCheck() ? 'उपयोगकर्ता नाम' : 'Username'}
            value={userName}
            onChangeText={val => {
              setUserName(val);
              // setAdhar(val)
              //   }
            }}
            style={styles.txtInput}
          />
          <TextInput
            keyboardType="numeric"
            placeholder={LanguageCheck() ? 'पिन नंबर' : 'Pin Number'}
            value={pinNumber}
            onChangeText={val => {
              setPinNumber(val);
              // setAdhar(val)
              //   }
            }}
            style={styles.txtInput}
          />
          <TextInput
            keyboardType="numeric"
            placeholder={LanguageCheck() ? 'मोबाइल नंबर' : 'Mobile Number'}
            value={mobile}
            onChangeText={val => {
              setMobile(val);
              // setAdhar(val)
              //   }
            }}
            style={styles.txtInput}
          />
          <TextInput
            placeholder={LanguageCheck() ? 'यूपीआई आईडी' : 'UPI Id'}
            value={upiId}
            onChangeText={val => {
              setUpiId(val);
              // setAdhar(val)
              //   }
            }}
            style={styles.txtInput}
          />
        </View>
        <CusButtom
          onpress={() => registerApi()}
          textStyle={styles.btnTxt}
          text={LanguageCheck() ? 'पंजीकरण करवाना' : 'Register'}
          BTNstyle={styles.generateOtpBtn}
        />
      </KeyboardAwareScrollView>
    </AuthFrame>
  );
}

const styles = StyleSheet.create({
  Phone: {
    marginTop: 20,
  },
  icon: {
    marginTop: 40,
    height: 140,
    width: 130,
    resizeMode: 'contain',
    alignItems: 'center',
    alignSelf: 'center',
  },
  logoContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 40,
  },
  headingTxt: {
    fontSize: 25,
    marginTop: 12,
    color: colors.white,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  heading2: {
    fontSize: 12,
    color: colors.white,
    marginBottom: 20,
    textAlign: 'center',
  },
  walletTxt: {
    fontSize: 20,
    color: colors.white,
    fontWeight: '600',
    marginTop: 30,
  },
  filltxt: {
    fontSize: 14,
    marginTop: 8,
    color: colors.white,
    marginHorizontal: '5%',
  },
  adartxt: {
    fontSize: 14,
    marginTop: 25,
    marginBottom: 10,
    color: colors.white,
  },
  txtInput: {
    height: 45,
    padding: 10,
    backgroundColor: colors.white,
    borderRadius: 5,
    color: colors.black,
    width: width * 0.9,
    marginBottom: 10,
  },
  txtiView: {
    alignItems: 'center',
  },
  generateOtpBtn: {
    width: '90%',
    height: 45,
    paddingVertical: 0,
    backgroundColor: colors.buttonBlue,
    borderColor: colors.buttonBlue,
    borderRadius: 50,
    marginTop: 25,
    alignSelf: 'center',
  },
  btnTxt: {
    fontSize: 18,
    color: colors.white,
    fontWeight: '600',
  },
  desc: {
    fontSize: 12,
    color: colors.white,
    paddingTop: 5,
    textAlign: 'center',
  },
  imageFlag: {
    height: 45,
    width: 80,
  },
  smallLogo: {
    height: 45,
    width: 45,
    resizeMode: 'contain',
  },
});
