// import { createStackNavigator } from '@react-navigation/stack';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {View, Text, StyleSheet, Dimensions} from 'react-native';
import Adhar from '../authScreens/Adhar';
import Verification from '../authScreens/Verification';
import React from 'react';
import PhoneNumber from '../authScreens/PhoneNumber';
import VerificationPhone from '../authScreens/VerificationPhone';
import InitialScreen from '../authScreens/InitialScreen';
import Language from '../authScreens/Language';
import FaceDetectionFunction from '../authScreens/FaceDetection';
const {height, width} = Dimensions.get('window');
const Stack = createNativeStackNavigator();
function AuthStackNav() {
  return (
    <Stack.Navigator
      initialRouteName={'InitialScreen'}
      screenOptions={{headerShown: false}}>
      <Stack.Screen name="Language" component={Language} />
      <Stack.Screen name="InitialScreen" component={InitialScreen} />
      <Stack.Screen name="registration" component={Adhar} />
      <Stack.Screen name="Verification" component={Verification} />
      <Stack.Screen name="login" component={PhoneNumber} />
      <Stack.Screen name="VerificationPhone" component={VerificationPhone} />

    </Stack.Navigator>
  );
}

export default AuthStackNav;
const styles = StyleSheet.create({
  container: {
    height: height,
    width: width,
  },
});
