import { createBottomTabNavigator, } from '@react-navigation/bottom-tabs';
import { Rtext } from '../common/Rtext';
import { View, Image } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context';
import Home from '../mainScreens/TabScreens/Home';
import AddMoneyy from '../mainScreens/TabScreens/AddMoneyy';
import Profile from '../mainScreens/TabScreens/Profile';
import { AllIcons, colors } from '../assets/common/Common';
import React from 'react';
import { useSelector } from 'react-redux';
const Tab = createBottomTabNavigator();

function BottomTabNav() {
    const language = useSelector(store => store?.auth?.language
        );
        const LanguageCheck = () => {
            return language === "hindi";
        }
    return (
        <Tab.Navigator screenOptions={{
            headerTintColor : colors.white,
            headerStyle :{
                backgroundColor: colors.appColor
            },
            headerShown: true, tabBarActiveTintColor: colors.bottomActiveTintColor, tabBarInactiveTintColor: colors.bottomInActiveTintColor, tabBarStyle: {
                // backgroundColor: GlobalStyles.colors.accent500,
                paddingTop: 7,
                borderTopLeftRadius: 24,
                borderTopRightRadius: 24,
        
                position: 'absolute',
                overflow: 'hidden',
            }
        }}>
            <Tab.Screen name={"Home"} component={Home} options={{
                tabBarLabel: 'Home',
                title : LanguageCheck()? "घर": "Home",
                tabBarLabel :  LanguageCheck()? "घर": "Home",
                tabBarIcon: ({ color, size }) => (

                    <Icons source={AllIcons.home} tintColor={color} size={size} />


                ),
            }} />
            {/* <Tab.Screen name="AddMoneyy" component={AddMoneyy} options={{
                tabBarLabel: 'Add Money',
                tabBarIcon: ({ color, size }) => (

                    <Icons source={AllIcons.addMoney} tintColor={color} size={size} />
                ),
            }} /> */}
            <Tab.Screen name={ "Profile"} component={Profile} options={{
                tabBarLabel: 'Profile',
                title : LanguageCheck()?"प्रोफ़ाइल" :  "Profile",
                tabBarLabel : LanguageCheck()?"प्रोफ़ाइल" :  "Profile",
                tabBarIcon: ({ color, size }) => (
                    <Icons source={AllIcons.profile} tintColor={color} size={size} />
                ),
            }} />
        </Tab.Navigator>
    );
}



const Icons = ({ source, tintColor, size }) => {
    return (
        <View>
            {
              tintColor === colors.bottomActiveTintColor &&              <View style={{ position: 'absolute', top: -10, left: -8, backgroundColor:colors.bottomActiveTintColor, height: 5, width: 40 }} />
            }

            <Image style={{ height: size, tintColor, width: size, resizeMode: 'contain' }} source={source} />
        </View>
    )
}

export default BottomTabNav;