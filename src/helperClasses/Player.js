

import { Player, Recorder } from '@react-native-community/audio-toolkit'
import { PermissionsAndroid, Platform } from 'react-native'

class PlayerAudio {
    player: Player;

    async startPlaying(path) {
        this.player = new Player(path);
        this.player.prepare((err) => {
            try {
                this.player.play()
            } catch (nerr) {
                console.log("nerr", nerr)
            }
        });
    }

    async stopPlaying() {
        this.player.stop();
        this.player.destroy();
    }
}

export const newPlayer = new PlayerAudio()
