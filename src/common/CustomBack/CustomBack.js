import { View, Text, StyleSheet, Image , TouchableOpacity } from 'react-native'
import React from 'react'
import { AllIcons, colors } from '../../assets/common/Common'
import { Rtext } from '../Rtext'
import { useNavigation } from '@react-navigation/native';
 const CustomBack= ({ name = "" }) => {
  const navigation = useNavigation()
  return (
    <TouchableOpacity onPress={() => navigation.goBack()} style={styles.buttonContainer}>
      <Image style={styles.backImg} source={AllIcons.back} />
      <Rtext style={ styles.backTxt}>{name}</Rtext>
    </TouchableOpacity>
  )
}
export default CustomBack;
const styles = StyleSheet.create({
  buttonContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop : 25,
    marginLeft : "2%"
  },
  backImg: {
    height: 24, width: 24, resizeMode: 'contain'
  },
  backTxt:{
fontSize : 18,
fontWeight : 'bold',
color: colors.white , marginLeft : 10
  }
  
})