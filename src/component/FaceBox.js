import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

function FaceBox({origin, name, size}) {
  const styles = {
    box: {
      height: size.height,
      width: size.width,
      left: origin.x,
      top: origin.y,
      position: 'absolute',
      borderWidth: 1,
      borderColor: '#e0301e',
      opacity: 0.5,
    },
    name: {
      fontSize: 10,
    },
  };

  return (
    <View style={styles.box}>
      <Text>{name}</Text>
    </View>
  );
}

export default FaceBox;
