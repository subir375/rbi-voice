import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import ReactNativeBiometrics, {BiometryTypes} from 'react-native-biometrics';
const rnBiometrics = new ReactNativeBiometrics({allowDeviceCredentials: true});
export const FingerPrint = async () => {
  let epochTimeSeconds = Math.round(new Date().getTime() / 1000).toString();
  let payload = epochTimeSeconds + 'some message';
  let result;

  return new Promise((resolve, reject) => {
    rnBiometrics.isSensorAvailable().then(async resultObject => {
      const {available, biometryType} = resultObject;
      if (available && biometryType === BiometryTypes.TouchID) {
        console.log('TouchID is supported');
      } else if (available && biometryType === BiometryTypes.FaceID) {
        console.log('FaceID is supported');
      } else if (available && biometryType === BiometryTypes.Biometrics) {
        console.log('Biometrics is supported');

        // rnBiometrics.createKeys().then(resultObject => {
        //   const {publicKey} = resultObject;
        //   console.log(publicKey);
        //   // sendPublicKeyToServer(publicKey);
        // });
        //  rnBiometrics.biometricKeysExist().then(resultObject => {
        //   console.log('biometricKeysExist ',resultObject);
        //   const {keysExist} = resultObject;

        //   if (keysExist) {
        //     console.log('Keys exist');
        //   } else {
        //     console.log('Keys do not exist or were deleted');
        //   }
        // });
        let res = await rnBiometrics
          .simplePrompt({
            promptMessage: 'Authentication Required',
            // payload: payload
          })
          .then(resultObject => {
            console.log('resultObject ', resultObject);
            const {success, signature} = resultObject;
            if (success) {
              result = true;
              resolve(result);
              // return result;
              // verifySignatureWithServer(signature, payload)
            } else {
              result = false;
              reject(result);
            }
          });
      } else {
        console.log('Biometrics not supported');
        reject(result);
      }
    });
  });
};

// export default FingerPrint;
