import React, {Component} from 'react';
import Tts from 'react-native-tts';

type Props = {};
class TextToSpeech extends Component<Props> {
  state = {
    voices: [],
    ttsStatus: 'initiliazing',
    selectedVoice: null,
    speechRate: 0.5,
    speechPitch: 1,
    text: 'This is an example text',
  };

  constructor(props) {
    super(props);
    Tts.addEventListener('tts-start', event =>
      this.setState({ttsStatus: 'started'}),
    );
    Tts.addEventListener('tts-finish', event =>
      this.setState({ttsStatus: 'finished'}),
    );
    Tts.addEventListener('tts-cancel', event =>
      this.setState({ttsStatus: 'cancelled'}),
    );
    Tts.setDefaultRate(this.state.speechRate);
    Tts.setDefaultPitch(this.state.speechPitch);
    Tts.getInitStatus().then(this.initTts);
  }

  initTts = async () => {
    const voices = await Tts.voices();
    const availableVoices = voices
      .filter(v => !v.networkConnectionRequired && !v.notInstalled)
      .map(v => {
        return {id: v.id, name: v.name, language: v.language};
      });
    let selectedVoice = null;
    if (voices && voices.length > 0) {
      selectedVoice = voices[0].id;
      try {
        await Tts.setDefaultLanguage(voices[0].language);
      } catch (err) {
        // My Samsung S9 has always this error: "Language is not supported"
        console.log(`setDefaultLanguage error `, err);
      }
      await Tts.setDefaultVoice(voices[0].id);
      this.setState({
        voices: availableVoices,
        selectedVoice,
        ttsStatus: 'initialized',
      });
    } else {
      this.setState({ttsStatus: 'initialized'});
    }
  };

  readText = async text => {
    Tts.addEventListener();
    Tts.stop();
    Tts.speak(text);
    // return Promise = new Promise((resolve, reject) => {
    //   Tts.addEventListener('tts-finish', event => resolve(true));
    //   Tts.addEventListener('tts-cancel', event => reject(false));
    // })
  };

  setSpeechRate = async rate => {
    await Tts.setDefaultRate(rate);
    this.setState({speechRate: rate});
  };

  setSpeechPitch = async rate => {
    await Tts.setDefaultPitch(rate);
    this.setState({speechPitch: rate});
  };

  onVoicePress = async voice => {
    try {
      await Tts.setDefaultLanguage(voice.language);
    } catch (err) {
      // My Samsung S9 has always this error: "Language is not supported"
      console.log(`setDefaultLanguage error `, err);
    }
    await Tts.setDefaultVoice(voice.id);
    this.setState({selectedVoice: voice.id});
  };
}

export const NewTextToSpeech = new TextToSpeech();
